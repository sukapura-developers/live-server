<?php
// app/helpers.php
function textClass($status, $last_message) {
  if ($last_message == 'is running') { // change text color based on the last message
    return ($status == 'success') ? 'text-success' : 'text-danger';
  }
  return ($status == 'failed') ? 'text-danger' : '';
}

function onlyEnabled($collection) { // filter the collection to only the one's which are enabled
  return $collection->filter(function($item) {
    return $item->enabled == 1;
  });
}

function minValue($checks) { // used for returning the oldest last_ran_at date
  return min(array_column($checks->toArray(), 'last_ran_at'));
}

function numberTextClass($type, $status, $text) { // change text color based on the threshold value
  // these maps to the treshold configs in the config/server-monitor.php`
  $configs = [
    'diskspace' => 'server-monitor.diskspace_percentage_threshold',
    'cpu' => 'server-monitor.cpu_usage_threshold',
    'memory' => 'server-monitor.memory_usage_threshold'
  ];

  preg_match('/(\d+)/', $text, $pieces); // get all the numbers in the text

  if (!empty($pieces)) {
    $number = (float) $pieces[0];
    $config = config($configs[$type]);
    return ($number >= $config['fail']) ? 'text-danger' : (($number >= $config['warning']) ? 'text-warning' : ''); // determine the class to add based on the current percentage value
  }

  return textClass($status, $text); // for the one's whose value isn't percentage based
}