  // resources/js/index.js
    // set the CSRF token generated in the page as a header value for all AJAX requests
    // https://laravel.com/docs/master/csrf
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      
      window.Visibility = require('visibilityjs'); // import the visibility.js library
      
      // subscribe to the live-monitor channel and listen to the finished.check event
      window.Echo.channel('live-monitor')
      .listen('.finished.check', (e) => {
      
        const { id, type, last_run_message, element_class, last_update, host_id } = e.message; // destructure the event data
      
        $(`#${id} .${type}`)
          .text(last_run_message)
          .removeClass('text-success text-danger text-warning')
          .addClass(element_class);
      
        $(`#${host_id}`).text(`Last update: ${last_update}`);
      });
      
      // next: add code for updating page visibility